//Exercicio 3 - Lista
public class Candidato {
	
	private int numero;
	private String nome;
	private int votos = 0;
	
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getVotos() {
		return votos;
	}
	public void setVotos(int votos) {
		this.votos = votos;
	}
	
	public void votar() {
		votos++;
	}
	
	
	public void Candidato(String Nome, int Numero) {
		this.nome = Nome;
		this.numero = Numero;
		votos = 0;
		
	}
	
	
	
}
