
class Funcionario extends Pessoa {
	// Atributos
	private String cargo;
	private long registro;
	private double salario;

	// Construtor
	public Funcionario(String n, String c, long r, double s) {
		nome = n;
		cargo = c;
		registro = r;
		salario = s;
	}

	// M�todo para imprimir dados do aluno
	public void print() {
		System.out.println("Nome: " + nome);
		System.out.println("Cargo: " + cargo);
		System.out.println("Registro: " + registro);
		System.out.println("Salario: " + salario);
	}
}