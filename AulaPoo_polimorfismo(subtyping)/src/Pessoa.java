
class Pessoa {
	// Atributos protected (para serem herdados)
	 protected String nome;
	 protected long identidade;

	 // Construtor
	 public Pessoa(String n, long i) {
	 dados(n, i);
	 }
	 // Construtor 2
	 public Pessoa() { }
	 // Metodo para passagem de dados
	 public void dados (String n, long id) {
	 nome = n;
	 identidade = id;
	 }
	 // Metodo para impressao de dados
	 public void print () {
	 System.out.println("Nome: " + nome); 
	}
}
