// Exercicio 1
import java.util.Scanner;

public class App {

	static final int MAX = 10;
	static int i = 0;

	static Pessoa[] lista = new Pessoa[MAX];

	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		int op;
		do {
			System.out.println("*** MENU PRINCIPAL ***");
			System.out.println("1-Incluir Pessoa");
			System.out.println("2-Listar Pessoas");
			System.out.println("3-Sair");
			System.out.println("Digite sua op��o: ");
			op = tecla.nextInt();

			switch (op) {
			case 1:
				incluirPessoa();
				break;
			case 2:
				listarPessoa();
				break;
			case 3:
				break;
			}
		} while (op != 3);
	}

	public static void incluirPessoa() {
		System.out.println("Digite o nome da pessoa:");
		String nome = tecla.next();

		lista[i++] = new Pessoa(nome);
		System.out.println("Pessoa inserida com suceeso!");

	}

	public static void listarPessoa() {
		System.out.println("Nomes: ");
		for (int i = 0; i < lista.length-1; i++) {
			if (lista[i] != null) {
				System.out.println(lista[i].getNome());
			} else {
				break;
			}
		}
	}

}
